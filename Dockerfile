#::::::::::::::
#Dockerfile_ready
#::::::::::::::
# The following image is based on shiny-verse, i.e. provides a shiny server
FROM eugloh/analycyte_base:latest

USER root

# On the host git clone the_current_repo and change dir
# Then copy all files from host to guest workdir
WORKDIR /tmp
COPY ./ ./

# Install packages with apt-get
RUN if [ -f "apt.txt" ]; then \
  apt-get update -qq; \
  apt-get -y --no-install-recommends install `grep -v "^#" apt.txt | tr '\n' ' '`; \
  fi

# Install an R environment
RUN if [ -f "install.R" ]; then \
  Rscript install.R; \
  fi

# Use shiny server and change its configuration
RUN cp shiny-server.conf /etc/shiny-server/shiny-server.conf;
WORKDIR /srv/shiny-server
RUN rm -fr *; \
  mkdir analycyte; \
  echo 'options("golem.app.prod" = TRUE); analycyte::run_app()' > analycyte/app.R;

# This last CMD instruction launches a R process
# Comment it in order to start the shiny-server instead
CMD R -e "options('shiny.port'=3838,shiny.host='0.0.0.0');library(analycyte);analycyte::run_app()"
